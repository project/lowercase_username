<?php

namespace Drupal\lowercase_username\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class LowercaseUsernameSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'lowercase_username_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['lowercase_username.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $formState = &$form_state;

    $config = $this->config('lowercase_username.settings');

    $form['username_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow dots (.)'),
      '#default_value' => $config->get('username.dots'),
    ];

    $form['username_hyphens'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow hyphens (-)'),
      '#default_value' => $config->get('username.hyphens'),
    ];

    $form['username_underscores'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow underscores (_)'),
      '#default_value' => $config->get('username.underscores'),
    ];

    $form['username_numbers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow numbers (0-9)'),
      '#default_value' => $config->get('username.numbers'),
    ];

    $form['username_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('username.description'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $formState = &$form_state;

    $this->config('lowercase_username.settings')
      ->set('username.dots', $formState->getValue('username_dots'))
      ->set('username.hyphens', $formState->getValue('username_hyphens'))
      ->set('username.underscores', $formState->getValue('username_underscores'))
      ->set('username.numbers', $formState->getValue('username_numbers'))
      ->set('username.description', $formState->getValue('username_description'))
      ->save();

    parent::submitForm($form, $formState);
  }

}
