<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * @param array $form
 * @param FormStateInterface $formState
 * @param string $formId
 */
function lowercase_username_form_user_form_alter(array &$form, FormStateInterface $formState, string $formId): void {
  if (isset($form['account']['name'])) {
    $config = \Drupal::config('lowercase_username.settings');
    $form['account']['name']['#description'] = $config->get('username.description');
  }

  $form['#validate'][] = 'lowercase_username_validate';
}

/**
 * @param array $form
 * @param FormStateInterface $formState
 */
function lowercase_username_validate(array &$form, FormStateInterface $formState): void {
  $username = $formState->getValue('name');
  $config = \Drupal::config('lowercase_username.settings');
  $pattern = 'a-z';

  if ($config->get('username.numbers')) {
    $pattern .= '0-9';
  }

  if ($config->get('username.dots')) {
    $pattern .= '\.';
  }

  if ($config->get('username.underscores')) {
    $pattern .= '_';
  }

  if ($config->get('username.hyphens')) {
    $pattern .= '-';
  }

  $pattern = '/[^' . $pattern . ']+/';

  if (preg_match($pattern, $username)) {
    $message = t('The username contains an illegal character.');
    $formState->setErrorByName('name', $message);
  }
}
